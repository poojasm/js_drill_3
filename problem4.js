function problem4(users) {
  try {
    /*-------using loops------- */
    // const programmers = {
    //   golang: [],
    //   javascript: [],
    //   python: []
    // };

    // for(let [person, details] of Object.entries(users)){
    //   if (details.desgination.toLowerCase().includes('golang')) {
    //     programmers.golang.push(person)
    //   }
    //   if (details.desgination.toLowerCase().includes('javascript')) {
    //     programmers.javascript.push(person)
    //   }
    //   if (details.desgination.toLowerCase().includes('python')) {
    //     programmers.python.push(person)
    //   }
    // }
    // return programmers;

    /*-----------using HOF--------- */
    const programmers = {
      golang: [],
      javascript: [],
      python: [],
    };
    const nameAndDetails = Object.entries(users);

    nameAndDetails.forEach((user) => {
      const [name, details] = user;
      if (details.desgination.toLowerCase().includes("golang"))
        programmers.golang.push(name);

      if (details.desgination.toLowerCase().includes("javascript"))
        programmers.javascript.push(name);

      if (details.desgination.toLowerCase().includes("python"))
        programmers.python.push(name);
    });
    return programmers;
  } catch (error) {
    console.log("Error");
  }
}

export default problem4;
