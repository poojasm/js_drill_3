function problem2(users) {
  try {
    /*-----------using loops------- */
    // const peopleInGermany = [];

    // for(let [person, details] of Object.entries(users)) {
    //   if (details.nationality.toLowerCase() == "germany")
    //       peopleInGermany.push(person)
    // }
    // return peopleInGermany;

    /*------------using HOF---------- */
    const nameAndDetails = Object.entries(users);

    const usersInGermany = nameAndDetails.reduce((acc, user) => {
      const [name, details] = user;
      if (details.nationality.toLowerCase() == "germany") acc.push(name);
      return acc;
    }, []);

    return usersInGermany;
  } catch (error) {
    console.log("Error");
  }
}

export default problem2;
