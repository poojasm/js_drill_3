function problem3(users) {
  try {
    /*----------using lopps--------- */
    // const masterDegreeHolders = [];

    // for (let [person, details] of Object.entries(users)) {
    //   if(details.qualification.toLowerCase().includes("masters")){
    //     masterDegreeHolders.push(person)
    //   }
    // }

    // return masterDegreeHolders;

    /*----------using HOF--------- */

    const nameAndDetails = Object.entries(users);
    const masterDegreeHolders = nameAndDetails.reduce((acc, user) => {
      const [name, details] = user;

      if (details.qualification.toLowerCase().includes("master"))
        acc.push(name);
      return acc;
    }, []);

    return masterDegreeHolders;
  } catch (error) {
    console.log("Error");
  }
}

export default problem3;
