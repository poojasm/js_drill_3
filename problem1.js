function problem1(users) {
  try {
    /*------------using loops----------- */
    // const videoGamePlayers = [];

    // for (let [person, details] of Object.entries(users)) {
    //   const interest = details.interests;

    //   for (let index = 0; index < interest.length; index++) {
    //     if (interest[index].toLowerCase().includes("video games")) {
    //       videoGamePlayers.push(person);
    //       break;
    //     }
    //   }
    // }
    // return videoGamePlayers;

    /*---------------using HOF-------------- */
    const videoGamePlayers = [];
    const doesPlayVideoGames = (data) =>
      data.interests.filter((value) =>
        value.toLowerCase().includes("video games")
      ).length;

    Object.entries(users).forEach((user) => {
      const [name, details] = user;
      const doesPlay = doesPlayVideoGames(details);

      if (doesPlay) videoGamePlayers.push(name);
    });
    return videoGamePlayers;
  } catch (error) {
    console.log("Error");
  }
}

export default problem1;
